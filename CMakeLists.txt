cmake_minimum_required(VERSION 2.8.8)
project(nrdf_gazebo_plugin)


find_package(gazebo REQUIRED)
find_package(manifold0 QUIET REQUIRED)

#set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${MANIFOLD_CXX_FLAGS}")

include_directories(
  ${GAZEBO_INCLUDE_DIRS}
  ${MANIFOLD_INCLUDE_DIRS}
  include
)

link_directories(
  ${GAZEBO_LIBRARY_DIRS}
  ${MANIFOLD_LIBRARY_DIRS}
)

list(
   APPEND CMAKE_CXX_FLAGS "${GAZEBO_CXX_FLAGS}"
)

set(
   CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${GAZEBO_CXX_FLAGS}"
)

file(
   GLOB rndf_gazebo_plugin_SRC "src/*.cc"
)

add_library( nrdf_gazebo_plugin SHARED ${rndf_gazebo_plugin_SRC} )

target_link_libraries(nrdf_gazebo_plugin ${GAZEBO_LIBRARIES} ${MANIFOLD_LIBRARIES})