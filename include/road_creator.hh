/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#ifndef ROAD_CREATOR_HH
#define ROAD_CREATOR_HH

#include <gazebo/gazebo.hh>
#include <gazebo/util/system.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/rendering/rendering.hh>
#include <gazebo/rendering/RenderTypes.hh>
#include <gazebo/transport/TransportTypes.hh>
#include <gazebo/transport/Publisher.hh>
#include <ignition/math.hh>

#include <iostream>
#include <string>
#include <vector>

#include "creator.hh"

namespace gazebo {

namespace gui {
/// \brief  Utility class to make road visuals on Gazebo
class RoadCreator {
  public:
    /// \brief Default constructor.
    RoadCreator();
    /// \brief Destructor.
    ~RoadCreator();
    /// \brief It initilizes the object creating a publisher to Gazebo
    void Init();
    /// \brief It creates an interpolated road with splines.
    /// It is recursive to match the minimum distance.
    /// \param[in] points the control points of the splines
    /// \param[in] distanceThreshold it is the maximum distance between
    /// two points of the road
    /// \return A vector of points that is created with splines and matches
    /// the maximum distance between all.
    std::vector<ignition::math::Vector3d> InterpolateRoad(
      const std::vector<ignition::math::Vector3d> &points,
      const double distanceThreshold);
    /// \brief It creates a road message
    /// \param[in] width the road width
    /// \param[in] name the road visual name
    /// \param[in] points vector of points that make the path of the road
    /// \return A road message pointer to send via the PublishRoadMessage
    /// function.
    gazebo::msgs::RoadPtr CreateRoadMessage(const double width,
      const std::string &name,
      const std::vector<ignition::math::Vector3d> &points);
    /// \brief It publishes a road message to the Gazebo
    /// server
    /// \param[in] roadMsg A message smart pointer reference to
    /// send via the publisher
    /// \param[in] parentVisual A visual smart pointer to reference
    /// the parent visual of the road.
    void PublishRoadMessage(const gazebo::msgs::RoadPtr &roadMsg,
      const gazebo::rendering::VisualPtr parentVisual);

  private:
    /// \brief We create publishers from it.
    transport::NodePtr node;
    /// \brief The publisher is used to send messages to the Gazebo server
    transport::PublisherPtr roadPublisher;

  protected:
};

}

}
#endif
