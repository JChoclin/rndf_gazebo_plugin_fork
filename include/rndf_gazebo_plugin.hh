/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/

#ifndef RNDF_GAZEBO_PLUGIN_HH
#define RNDF_GAZEBO_PLUGIN_HH

#include <gazebo/gazebo.hh>
#include <gazebo/util/system.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/rendering/rendering.hh>
#include <gazebo/rendering/RenderTypes.hh>
#include <gazebo/transport/TransportTypes.hh>
#include <gazebo/transport/Publisher.hh>
#include <gazebo/gui/GuiIface.hh>
#include <gazebo/gui/EntityMaker.hh>
#include <gazebo/gui/GuiEvents.hh>
#include <gazebo/math/Vector3.hh>
#include <gazebo/common/common.hh>
#include <gazebo/common/Console.hh>
#include <gazebo/rendering/UserCamera.hh>
#include <gazebo/math/Quaternion.hh>
#include <gazebo/common/MouseEvent.hh>
#include <gazebo/common/Exception.hh>
#include <sdf/Element.hh>
#include <ignition/math.hh>
#include <ignition/msgs.hh>
#include <ignition/transport.hh>


#include <manifold/rndf/RNDF.hh>
#include <manifold/rndf/Segment.hh>
#include <manifold/rndf/Lane.hh>
#include <manifold/rndf/Waypoint.hh>
#include <manifold/rndf/Exit.hh>
#include <manifold/rndf/UniqueId.hh>
#include <manifold/rndf/Zone.hh>
#include <manifold/rndf/Perimeter.hh>
#include <manifold/rndf/ParkingSpot.hh>

#include <fstream>
#include <streambuf>
#include <sstream>
#include <chrono>
#include <thread>
#include <string>
#include <vector>

#include "road_creator.hh"
#include "waypoint_creator.hh"
#include "camera_controller.hh"
#include "projection.hh"
#include "perimeter_creator.hh"

#define DEBUG_MSGS
#ifdef DEBUG_MSGS
  #define DEBUG_MSG(str) do { std::cout << str << std::endl; } while (false)
#else
  #define DEBUG_MSG(str) do { } while (false)
#endif

#define DEFAULT_INTERPOLATION_DISTANCE  10.0

namespace gazebo {

namespace gui {

/// \brief  It is the base class of the plugin which handles the RNDF
/// parsing and sends messages to Gazebo to build a city.
class DynamicRender : public SystemPlugin{
  public:
    /// \brief Default constructor.
    DynamicRender();
    /// \brief Destructor.
    virtual ~DynamicRender();
    /// \brief It loads the Update event handler and tries to load the
    ///  RNDF file.
    void Load(int argc, char ** argv);

    void OnPublishWaypoints(const manifold::rndf::UniqueId &wpId);

  private:

    /// \brief It initializes utility classes such as the road creator and the
    /// the waypoint creator.
    void Init();
    /// \brief Callback handler of the update event.
    void Update();
    /// \brief It loads a sample RNDF file using Manifold Library
    /// \throws gazebo::common::Exception If the RNDF file isn't successfully
    /// loaded
    void LoadRNDFFile();
    /// \brief It prints the RNDF file stats using Manifold Library
    void PrintRNDFStats();
    /// \brief It loads the segments in Gazebo
    /// \param[in] segments It is the vector of segments to draw
    void LoadSegments(std::vector<manifold::rndf::Segment> &segments);
    /// \brief It loads the lanes in Gazebo
    /// \param[in] segmentParent It is the parent segment id
    /// \param[in] lanes It is the vector of lanes to draw
    void LoadLanes(const int segmentParent,
      std::vector<manifold::rndf::Lane> &lanes);
    /// \brief Wrapper to call all the junctions of all the lanes
    /// \param[in] lanes It is a vector of lanes to evaluate its
    /// exits and entries
    void LoadJunctions(std::vector<manifold::rndf::Lane> &lanes);
    /// \brief It draws all the junctions of a lane
    /// \param[in] It draws a straight lane between the entry and exit point
    void LoadJunction(const manifold::rndf::Lane &lane);
    /// \brief It loads all the junctions of a perimeter
    /// \param[in] perimeter The reference to the perimeter
    void LoadJunction(const manifold::rndf::Perimeter &perimeter);
    /// \brief It creates road segments between the entry and the exit
    /// points of the exit object
    /// \param[in] exit It is the reference to the exit object.
    /// \param[in] scene It is the pointer to the current scene.
    void CreateJunction(manifold::rndf::Exit &exit,
      rendering::ScenePtr scene);
    /// \brief It loads the waypoints in Gazebo
    /// \param[in] segmentParent It is the parent segment id
    /// \param[in] laneParent It is the parent lane id
    /// \param[in] waypoints It is the vector of waypoints to draw
    void LoadWaypoints(const int segmentParent,
      const int laneParent,
      std::vector<manifold::rndf::Waypoint> &waypoints);
    /// \brief It loads waypoints setting a custom type to all
    /// of them
    /// \param[in] segmentParent It is the parent segment id
    /// \param[in] laneParent It is the parent lane
    /// \param[in] waypoints A STL vector containing all the waypoints
    /// \param[in] type The type of waypoint to represent
    void LoadWaypoints(const int segmentParent,
      const int laneParent,
      std::vector<manifold::rndf::Waypoint> &waypoints,
      const gazebo::gui::WayPointCreator::Type type);
    /// \brief It loads a waypoint setting a custom type
    /// \param[in] segmentParent It is the parent segment id
    /// \param[in] laneParent It is the parent lane
    /// \param[in] waypoint A reference to the waypoint
    /// \param[in] type The type of waypoint to represent
    void LoadWaypoint(const int segmentParent,
      const int laneParent,
      manifold::rndf::Waypoint &waypoint,
      ignition::math::Pose3d &pose,
      const gazebo::gui::WayPointCreator::Type type);
    /// \brief It gets the pose of a waypoint given a waypoint and the
    /// next waypoint.
    /// \param[in] waypoint It is the waypoint to retrive its pose
    /// \param[in] nextWaypoint It is the following waypoint in the lane
    /// \return The pose of the waypoint
    ignition::math::Pose3d GetWaypointPose(
      manifold::rndf::Waypoint &waypoint,
      manifold::rndf::Waypoint &nextWaypoint);
    /// \brief It retrieves the poses of a list of waypoints supposed
    /// that all waypoinst are in the same lane.
    /// \param[in] waypoints It is vector of waypoints to retrieve the
    /// the poses
    /// \return A vector of poses of the waypoints
    std::vector<ignition::math::Pose3d> GetWaypointsPoses(
      std::vector<manifold::rndf::Waypoint> &waypoints);
    /// \brief It finds a waypoint by its unique id
    /// \param[in] waypointId It is an object that represents the id of the
    /// waypoint inside the RNDF file.
    /// \return The waypoint if that matches the waypoint id
    /// \throws gazebo::common::Exception if the waypoint is not found.
    manifold::rndf::Waypoint GetWaypointByUniqueId(
      const manifold::rndf::UniqueId &waypointId);
    /// \brief It gets the lane reference by a valid waypoint into it
    /// \param[in] waypointId It is the reference to the waypoint
    /// \return A reference to the lane if it was found
    /// \throws gazebo::common::Exception if the lane is not found.
    manifold::rndf::Lane& GetLaneByUniqueId(
      const manifold::rndf::UniqueId &waypointId);
    /// \brief If searches for a waypoint inside the segments
    /// \param[in] waypointId A representation of the id of the
    /// waypoint
    /// \return A copy of the Waypoint
    /// \throws gazebo::common::Exception if the waypoint is not found.
    manifold::rndf::Waypoint GetWaypointInSegmentsByUniqueId(
      const manifold::rndf::UniqueId &waypointId);
    /// \brief If searches for a waypoint inside the zones
    /// \param[in] waypointId A representation of the id of the
    /// waypoint
    /// \return A copy of the Waypoint
    /// \throws gazebo::common::Exception if the waypoint is not found.
    manifold::rndf::Waypoint GetWaypointInZonesByUniqueId(
      const manifold::rndf::UniqueId &waypointId);
    /// \brief It creates a lane name like 'segment_SId_'
    /// \param[in] segmentId It is the segment Id
    /// \return A std::string in the form 'segment_SId_'
    std::string createSegmentName(const int segmentId);
    /// \brief It creates a lane name like 'lane_SId_LId'
    /// \param[in] segmentParentId It is the segment parent Id
    /// \param[in] laneId It is the lane Id
    /// \return A std::string in the form 'lane_SId_LId'
    std::string createLaneName(const int segmentParentId, const int laneId);
    /// \brief It creates a waypoint name like 'waypoint_SId_LId_WId'
    /// \param[in] segmentParentId It is the segment parent id
    /// \param[in] laneParentId It is the lane parent id
    /// \return A std::string in the form 'waypoint_SId_LId_WId'
    std::string createWaypointName(const int segmentParentId,
      const int laneParentId,
      const int waypointId);
    /// \brief It creates a name for the perimeter
    /// \param[in] zoneId The id of the parent zone
    /// \returns A string with the format 'perimeter_XX' where XX is
    /// zoneId value.
    std::string CreatePerimeterName(const int zoneId);
    /// \brief It creates the junction name given the entry and exit point
    /// \details The format is "junction_X1.Y1.Z1_X2.Y2.Z2" where X.Y.Z
    /// with 1 are from the exit and with 2 from the entry
    /// \param[in] exitId It is the unique id of the exit
    /// \param[in] entryId It is the unique id of the entry
    /// \return A string with the format explained in the details.
    std::string CreateJunctionName(manifold::rndf::UniqueId &exitId,
      manifold::rndf::UniqueId &entryId);
    /// \brief It parses the argument list and loads the configuration
    /// \param[in] argc it is the number of strings in argv argument.
    /// \param[in] argv it is a vector with the strings sent as parameters
    /// \throws gazebo::common::Exception if file path is wrong or missing.
    void parseArgumentList(const int argc, char **argv);
    /// \brief It computes the lane control points with
    /// interpolation if needed.
    /// \param[in] lane It is the lane reference
    /// \return A vector with the position of the control points.
    std::vector<ignition::math::Vector3d> GetLanePoints(
      manifold::rndf::Lane &lane);
    /// \brief It parses the SDF file to get the plugin arguments
    /// \param[in] sdfFilePath This is the file path of the sdf world
    /// \throws gazebo::Exception in case there is a problem
    /// finding the plugin node.
    void ParseSDFFile(const std::string &sdfFilePath);
    /// \brief It loads the sdf file into gazebo through the /factory topic
    void SpawnModels();
    /// \brief It loads all the zones attributes.
    /// \param[in] zones A reference to a vector containing the zones
    void LoadZones(std::vector<manifold::rndf::Zone> &zones);
    /// \brief It loads the waypoints, the polygon of the perimeter
    /// and the junctions to it.
    /// \param[in] zoneId It is the id of its paren zone.
    /// \param[in] perimeter A reference to the perimeter.
    void LoadPerimeter(const int zoneId,
      manifold::rndf::Perimeter &perimeter);
    /// \brief It loads the waypoints of the parking spots
    /// \param[in] zoneId The id of the spot's zone.
    /// \param[in] spots A reference to a vector containing the zones.
    void LoadParkingSpots(const int zoneId,
      std::vector<manifold::rndf::ParkingSpot> &spots);
    /// \brief It creates a perimeter polygon
    /// \param[in] waypoints A reference to the waypoints that define the
    /// verteces of the waypoints.
    void CreatePerimeter(
      std::vector<manifold::rndf::Waypoint> &waypoints);
    /// \brief It keeps track of the times the Update function is called
    int count;
    /// \brief It holds the visual message that is created to create waypoints
    msgs::Visual *visualMsg;
    /// \brief It keeps the connections of the handlers like Update function
    std::vector<event::ConnectionPtr> connections;
    /// \brief It avoids the creation of the road more than once
    bool doItOnce;
    /// \brief Utiliy object that creates roads
    gazebo::gui::RoadCreator roadCreator;
    /// \brief Utility object that creates waypoints
    gazebo::gui::WayPointCreator waypointCreator;
    /// \brief Manifold object tha loads into memory the RNDF file
    std::shared_ptr<manifold::rndf::RNDF> rndfInfo;

    /// \brief It is the parsed argument to know if waypoints are required
    bool printWaypoints;
    /// \brief It is the parsed argument to know if waypoints are required
    bool printLanes;
    /// \brief It is the parsed RNDF file path argument
    std::string filePath;
    /// \brief This is the interpolation distance parameter for the roads.
    double interpolationDistance;
    /// \brief Wrapper to control the camera
    gazebo::gui::CameraController cameraController;
    /// \brief It has the projection methods to convert coordinates
    gazebo::gui::Projection projection;
    /// \brief We create publishers from it.
    transport::NodePtr node;
    /// \brief It creates a publisher on the ~/factory topic
    transport::PublisherPtr makerPub;
    // \brief It keeps track of the sdf file representation
    sdf::SDFPtr sdfPtr;

    gazebo::gui::PerimeterCreator perimeterCreator;

  protected:
};

  // Register this plugin with the simulator
  GZ_REGISTER_SYSTEM_PLUGIN(DynamicRender)
  // GZ_REGISTER_WORLD_PLUGIN(DynamicRender)
}
}

#endif
