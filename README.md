# RNDF gazebo plugin

** Gazebo plugin for RNDF visualization **

  [http://bitbucket.org/JChoclin/rndf_gazebo_plugin](http://bitbucket.org/JChoclin/rndf_gazebo_plugin)

## Continuous integration

Please refer to the [Bitbucket Pipelines](https://bitbucket.org/JChoclin/rndf_gazebo_plugin/addon/pipelines/home#!/).

## Dependencies

The following dependencies are required to compile manifold from source:

 - cmake
 - git
 - cppcheck
 - C++ compiler with c++11 support (eg. GCC>=4.8).
 - Gazebo on this [commit](https://bitbucket.org/osrf/gazebo/commits/2b49dbedff87910898d507c09135e1f078c40f59)
 - Docker. You can work with [this](https://github.com/ekumenlabs/terminus/tree/master/docker) docker image and check this [issue](https://github.com/ekumenlabs/terminus/issues/77)

1. Install the build dependencies:

    ```
    sudo apt-get install build-essential cmake git cppcheck
    ```

    ```
    curl -ssL http://get.gazebosim.org | sh
    ```

Then, you can run Gazebo to check your installation:

    ```
    gazebo
    ```

Link the pre-commit hook:

    ```
    ln tools/pre-commit.sh .git/hooks/pre-commit
    ```
    
## Installation

Standard installation can be performed in UNIX systems using the following steps (or in the Docker container):

 - mkdir build/
 - cd build/
 - cmake -DCMAKE_PREFIX_PATH=/tmp/manifold/build/ ..
 - make

If you are working with the docker container, it will be necessary to change the ownership of the library, so just run:

    ```
    chown gazebo libnrdf_gazebo_plugin.so
    ```

## Run on the docker container or in your UNIX system:

    ```
    gazebo --verbose -g ./libnrdf_gazebo_plugin.so -f <world_file>
    ```
Use:

 * --verbose: prints internal Gazebo messages and the plugin messages too.
 * -g: tells Gazebo to include the following library with the server.
 * -f: it includes the world file description.

 ### World file plugin description

 Your world file should have the following node inside the world node:

    ```
    <plugin name='rndf_plugin' filename='-'>
      <rndf>../example/darpa.rndf</rndf>
      <lanes>true</lanes>
      <waypoints>true</waypoints>
    </plugin>
    ```

The tag rndf is mandatory. It includes the file path of the RNDF file. The lanes and waypoint tags are optional and its content should be true or false indicating if you want to print the lanes and or the waypoints.  

