/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#include "waypoint_creator.hh"

namespace gazebo {

namespace gui {

WayPointCreator::WayPointCreator() {
}

WayPointCreator::~WayPointCreator() {
  node->Fini();
  node.reset();
  visPub.reset();
}

void WayPointCreator::Init() {
  node = transport::NodePtr(new transport::Node());
  node->Init();
  visPub = node->Advertise<msgs::Visual>("~/visual");
}


gazebo::msgs::VisualPtr WayPointCreator::CreateWayPointMessage(
  const std::string &name,
  const std::string &parentName,
  const ignition::math::Pose3d &pose,
  const double radius,
  const ignition::math::Vector3i &rgbColor,
  const WayPointCreator::Type type) {
    gazebo::msgs::VisualPtr visualMsg;
    visualMsg.reset(new gazebo::msgs::Visual);

    visualMsg->set_name(name);
    visualMsg->set_parent_name(parentName);
    visualMsg->set_visible(true);
    visualMsg->set_is_static(true);
    visualMsg->set_type(gazebo::msgs::Visual_Type_VISUAL);
    // Pose settings
    gazebo::msgs::Pose *_pose = visualMsg->mutable_pose();
    gazebo::msgs::Vector3d *_position = _pose->mutable_position();
    _position->set_x(pose.Pos().X());
    _position->set_y(pose.Pos().Y());
    _position->set_z(Creator::DEFAULT_HEIGHT + pose.Pos().Z() + 0.5);
    gazebo::msgs::Quaternion *_orientation = _pose->mutable_orientation();
    _orientation->set_x(pose.Rot().X());
    _orientation->set_y(pose.Rot().Y());
    _orientation->set_z(pose.Rot().Z());
    _orientation->set_w(pose.Rot().W());
    // Geometry settings
    gazebo::msgs::Geometry *_geometry = visualMsg->mutable_geometry();

    switch (type) {
      case WayPointCreator::Type::DEFAULT:
      case WayPointCreator::Type::CHECKPOINT:
      case WayPointCreator::Type::ENTRY:
      case WayPointCreator::Type::EXIT:
        CreateArrowGeometry(_geometry, radius);
      break;
      case WayPointCreator::Type::SPOT:
      case WayPointCreator::Type::PERIMETER:
      case WayPointCreator::Type::STOP:
        CreateCircleGeometry(_geometry, radius);
      break;
    }

    return visualMsg;
}

void WayPointCreator::CreateCircleGeometry(gazebo::msgs::Geometry *_geometry,
  const double radius) {
    _geometry->set_type(gazebo::msgs::Geometry_Type_CYLINDER);
    gazebo::msgs::CylinderGeom *_cylinderGeometry =
      _geometry->mutable_cylinder();
    _cylinderGeometry->set_radius(radius);
    _cylinderGeometry->set_length(0.1);
}

void WayPointCreator::CreateArrowGeometry(gazebo::msgs::Geometry *_geometry,
    const double radius) {
      _geometry->set_type(gazebo::msgs::Geometry_Type_POLYLINE);
      gazebo::msgs::Polyline *_polyline = _geometry->add_polyline();
      _polyline->set_height(0.0);
      gazebo::msgs::Vector2d *_point;
      _point = _polyline->add_point();
      _point->set_x(0.0);
      _point->set_y(2.0 * radius);
      _point = _polyline->add_point();
      _point->set_x(2.0 * radius);
      _point->set_y(0.0);
      _point = _polyline->add_point();
      _point->set_x(1.0 * radius);
      _point->set_y(0.0);
      _point = _polyline->add_point();
      _point->set_x(1.0 * radius);
      _point->set_y(-2.0 * radius);
      _point = _polyline->add_point();
      _point->set_x(-1.0 * radius);
      _point->set_y(-2.0 * radius);
      _point = _polyline->add_point();
      _point->set_x(-1.0 * radius);
      _point->set_y(0.0);
      _point = _polyline->add_point();
      _point->set_x(-2.0 * radius);
      _point->set_y(0.0);
}

void WayPointCreator::PublishVisualMessage(
  const gazebo::msgs::VisualPtr &visualMsg) {
    visPub->Publish(*visualMsg);
}

}
}
