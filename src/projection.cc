/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#include "projection.hh"

namespace gazebo {

namespace gui {

Projection::Projection() {
}

Projection::~Projection() {
}

ignition::math::SphericalCoordinates Projection::GetCenterOfThePlane() {
  return center;
}

ignition::math::Vector3d Projection::GetWaypointLocationInECEF(
  manifold::rndf::Waypoint &waypoint) {
  ignition::math::SphericalCoordinates origin(
    ignition::math::SphericalCoordinates::EARTH_WGS84);
  ignition::math::SphericalCoordinates &waypointLocation = waypoint.Location();
  ignition::math::Vector3d waypointCoordinates(
    waypointLocation.LatitudeReference().Radian(),
    waypointLocation.LongitudeReference().Radian(),
    waypointLocation.ElevationReference());
  return origin.PositionTransform(waypointCoordinates,
    ignition::math::SphericalCoordinates::SPHERICAL,
    ignition::math::SphericalCoordinates::ECEF);
}

ignition::math::Vector3d Projection::GetWaypointLocationInGlobal(
  manifold::rndf::Waypoint &waypoint) {
  ignition::math::SphericalCoordinates &waypointLocation =
    waypoint.Location();
  ignition::math::Vector3d waypointCoordinates(
    waypointLocation.LatitudeReference().Radian(),
    waypointLocation.LongitudeReference().Radian(),
    waypointLocation.ElevationReference());
  return center.PositionTransform(waypointCoordinates,
    ignition::math::SphericalCoordinates::SPHERICAL,
    ignition::math::SphericalCoordinates::GLOBAL);
}

ignition::math::Vector3d Projection::GetWaypointLocationInSpherical(
manifold::rndf::Waypoint &waypoint) {
  ignition::math::SphericalCoordinates &waypointLocation =
    waypoint.Location();
  return ignition::math::Vector3d(
    waypointLocation.LatitudeReference().Radian(),
    waypointLocation.LongitudeReference().Radian(),
    waypointLocation.ElevationReference());
}

void Projection::GetRNDFSpaceLimits(
  std::vector<manifold::rndf::Segment> &segments) {
  // Preload values before finding the maximun and minimum
  {
    manifold::rndf::Segment &segment = segments[0];
    manifold::rndf::Lane &lane = segment.Lanes()[0];
    manifold::rndf::Waypoint &waypoint = lane.Waypoints()[0];
    ignition::math::Vector3d position =
      GetWaypointLocationInSpherical(waypoint);
    lat_min = lat_max = position.X();
    long_min = long_max = position.Y();
    elev_min = elev_max = position.Z();
  }
  GenerateExtents(segments,
    &Projection::GetWaypointLocationInSpherical,
    &Projection::CheckCityLimitsLatLong);
  // Get the coordinates that matches center of the extents
  lat_center = (lat_max + lat_min) / 2.0;
  long_center = (long_max + long_min) / 2.0;
  elev_center = (elev_max + elev_min) / 2.0;
  GenerateCenterOfThePlane();

  // Preload values before finding the maximun and minimum
  ignition::math::Vector3d min = center.PositionTransform(
    ignition::math::Vector3d(lat_min, long_min, elev_min),
    ignition::math::SphericalCoordinates::SPHERICAL,
    ignition::math::SphericalCoordinates::GLOBAL);
  ignition::math::Vector3d max = center.PositionTransform(
    ignition::math::Vector3d(lat_max, long_max, elev_max),
    ignition::math::SphericalCoordinates::SPHERICAL,
    ignition::math::SphericalCoordinates::GLOBAL);
  x_min = min.X();
  y_min = min.Y();
  z_min = min.Z();
  x_max = max.X();
  y_max = max.Y();
  z_max = max.Z();
}

void Projection::GenerateExtents(std::vector<manifold::rndf::Segment> &segments,
  ignition::math::Vector3d(Projection::*WayPointLocationFunctor)
    (manifold::rndf::Waypoint &w),
  void(Projection::*CityLimitCheckFunctor)(const ignition::math::Vector3d &p)) {
    for (uint i = 0; i < segments.size(); i++) {
      manifold::rndf::Segment &segment = segments[i];
      std::vector<manifold::rndf::Lane> &lanes = segment.Lanes();
      for (uint j = 0; j < lanes.size(); j++) {
        manifold::rndf::Lane &lane = lanes[j];
        std::vector<manifold::rndf::Waypoint> &waypoints = lane.Waypoints();
        for (uint k = 0; k < waypoints.size(); k++) {
          manifold::rndf::Waypoint &waypoint = waypoints[k];
          ignition::math::Vector3d position =
            (this->*WayPointLocationFunctor)(waypoint);
          (this->*CityLimitCheckFunctor)(position);
        }
      }
    }
}

double Projection::GetXMax() {
  return x_max;
}

double Projection::GetXMin() {
  return x_min;
}

double Projection::GetYMax() {
  return y_max;
}

double Projection::GetYMin() {
  return y_min;
}

double Projection::GetZMax() {
  return z_max;
}

double Projection::GetZMin() {
  return z_min;
}

ignition::math::Vector3d Projection::GetMin() {
  return ignition::math::Vector3d(x_min, y_min, z_min);
}

ignition::math::Vector3d Projection::GetMax() {
  return ignition::math::Vector3d(x_max, y_max, z_max);
}
void Projection::CheckCityLimitsLatLong(
  const ignition::math::Vector3d &position) {
    lat_min = std::min(lat_min, position.X());
    lat_max = std::max(lat_max, position.X());
    long_min = std::min(long_min, position.Y());
    long_max = std::max(long_max, position.Y());
    elev_min = std::min(elev_min, position.Z());
    elev_max = std::max(elev_max, position.Z());
}

void Projection::CheckCityLimits(const ignition::math::Vector3d &position) {
    x_min = std::min(x_min, position.X());
    x_max = std::max(x_max, position.X());
    y_min = std::min(y_min, position.Y());
    y_max = std::max(y_max, position.Y());
    z_min = std::min(z_min, position.Z());
    z_max = std::max(z_max, position.Z());
}

void Projection::GenerateCenterOfThePlane() {
  center = ignition::math::SphericalCoordinates(
    ignition::math::SphericalCoordinates::EARTH_WGS84,
    ignition::math::Angle(lat_center),
    ignition::math::Angle(long_center),
    elev_center,
    ignition::math::Angle(0.0));
}

}
}
