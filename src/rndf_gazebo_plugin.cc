/*
 * Copyright (C) 2016 Open Source Robotics Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
*/
#include "rndf_gazebo_plugin.hh"

namespace gazebo {

namespace gui {

DynamicRender::DynamicRender() {
  doItOnce = false;
  printWaypoints = false;
  printLanes = false;
  interpolationDistance = DEFAULT_INTERPOLATION_DISTANCE;
}

DynamicRender::~DynamicRender() {
  this->node->Fini();
  this->node.reset();
  this->rndfInfo.reset();
  this->makerPub.reset();
}

void DynamicRender::Load(int argc, char ** argv) {
  parseArgumentList(argc, argv);
  ParseSDFFile(filePath);
  // Bind update function
  this->connections.push_back(
    event::Events::ConnectPreRender(
      boost::bind(&DynamicRender::Update, this)));

  LoadRNDFFile();
  PrintRNDFStats();
  projection.GetRNDFSpaceLimits(rndfInfo->Segments());
}

void DynamicRender::parseArgumentList(const int argc, char **argv) {
  for (int i = 0; i < argc; i++) {
    if (std::strcmp(argv[i], "-f") == 0) {
      // Check argument list size
      if (i + 1 >= argc) {
        gzthrow("File path missing.");
        return;
      }
      // Check file path not starting with '-'
      if (argv[i+1][0] == '-') {
        gzthrow("File path wrong or missing.");
        return;
      }
      filePath = argv[i+1];
      i++;
    }
    else if (std::strcmp(argv[i], "-w") == 0)
      printWaypoints = true;
    else if (std::strcmp(argv[i], "-l") == 0)
      printLanes = true;
  }

  if (filePath == "") {
    gzthrow("File path wrong or missing.");
    return;
  }

  DEBUG_MSG("--File path: " + filePath);
  DEBUG_MSG("--Print waypoints: " + std::to_string(printWaypoints));
  DEBUG_MSG("--Print lanes: " + std::to_string(printLanes));
}

void DynamicRender::ParseSDFFile(const std::string &sdfFilePath) {
  std::ifstream sdfStream(sdfFilePath);
  std::string sdfContent;
  sdfStream.seekg(0, std::ios::end);
  sdfContent.reserve(sdfStream.tellg());
  sdfStream.seekg(0, std::ios::beg);
  sdfContent.assign((std::istreambuf_iterator<char>(sdfStream)),
              std::istreambuf_iterator<char>());
  // This code gets the plugin node from the sdf file and
  // searches for different properties inside it that are
  // interesting for the plugin. In case there is no world or
  // plugin node an exception is raised.
  // 'rnfd' contains the path to the RNDF file. In case we cannot
  // find 'rndf' tag, an exception is raised.
  // 'lanes' contains a boolean indicating if we should draw them
  // 'waypoints' contains a boolean indicating if we should draw
  // them
  sdfPtr.reset(new sdf::SDF());
  sdfPtr->SetFromString(sdfContent);
  sdf::ElementPtr rootPtr = sdfPtr->Root();
  if (rootPtr->HasElement("world")) {
    sdf::ElementPtr worldPtr = rootPtr->GetElement("world");
    if (worldPtr->HasElement("plugin")) {
      sdf::ElementPtr pluginPtr = worldPtr->GetElement("plugin");
      // Parsing sdf arguments
      if (pluginPtr->HasElement("rndf")) {
        filePath = pluginPtr->Get<std::string>("rndf");
      }
      else
        gzthrow("There is no rndf tag inside rndf node.");
      if (pluginPtr->HasElement("lanes")) {
        printLanes = pluginPtr->Get<bool>("lanes");
      }
      if (pluginPtr->HasElement("waypoints")) {
        printWaypoints = pluginPtr->Get<bool>("waypoints");
      }
    }
    else
      gzthrow("There is no plugin node");
  }
  else
    gzthrow("There is no world node");
}

void DynamicRender::SpawnModels() {
  sdf::ElementPtr rootPtr = sdfPtr->Root();
  msgs::Factory msg;
  msg.set_sdf(rootPtr->ToString(""));
  this->makerPub->Publish(msg);
}

void DynamicRender::Init() {
  node = transport::NodePtr(new transport::Node());
  // Intialize internal variables
  roadCreator.Init();
  waypointCreator.Init();

  this->makerPub = this->node->Advertise<msgs::Factory>("~/factory");
  count = 0;
}

void DynamicRender::Update() {
  if (count == 1) {
    gzmsg << "Start to spawn models ..." << std::endl;
    SpawnModels();
    gzmsg << "Finish to spawn models ..." << std::endl;
  }
  if (count > 10) {
    if (doItOnce == false) {
      // Draw the lanes and waypoints and adjust the camera
      LoadZones(rndfInfo->Zones());
      LoadSegments(rndfInfo->Segments());
      cameraController.SetExtents(projection.GetMin(),
        projection.GetMax());
      cameraController.AdjustCamera();
      doItOnce = true;
    }
  }
  else
    count++;
}

void DynamicRender::LoadRNDFFile() {
  rndfInfo.reset(new manifold::rndf::RNDF(std::string(filePath)));
  if (!rndfInfo->Valid()) {
    gzthrow(std::string("File [") + filePath + std::string("] is invalid") );
  }
}

void DynamicRender::PrintRNDFStats() {
  // Show stats.
  std::cout << "Name:               [" << rndfInfo->Name() << "]" << std::endl;
  if (!rndfInfo->Version().empty()) {
    std::cout << "Version:            ["
      << rndfInfo->Version() << "]"
      << std::endl;
  }
  if (!rndfInfo->Date().empty()) {
    std::cout << "Creation date:      ["
      << rndfInfo->Date() << "]"
      << std::endl;
  }
  std::cout << "Number of segments: " << rndfInfo->NumSegments() << std::endl;
  std::cout << "Number of zones:    " << rndfInfo->NumZones() << std::endl;
}

void DynamicRender::LoadSegments(
  std::vector<manifold::rndf::Segment> &segments) {
    if (printLanes) {
      for (uint i = 0; i < segments.size(); i++) {
        manifold::rndf::Segment &segment = segments[i];
        LoadLanes(segment.Id(), segment.Lanes());
      }
      for (uint i = 0; i < segments.size(); i++) {
        LoadJunctions(segments[i].Lanes());
      }
    }
    if (printWaypoints) {
      for (uint i = 0; i < segments.size(); i++) {
        manifold::rndf::Segment &segment = segments[i];
        std::vector<manifold::rndf::Lane> &lanes = segment.Lanes();
        for (uint j = 0; j < lanes.size(); j++) {
          LoadWaypoints(segment.Id(), lanes[j].Id(),
            lanes[j].Waypoints());
        }
      }
    }
}

void DynamicRender::LoadLanes(const int segmentParent,
  std::vector<manifold::rndf::Lane> &lanes) {
  rendering::ScenePtr scene = rendering::get_scene();

  for (uint i = 0; i < lanes.size(); i++) {
    manifold::rndf::Lane &lane = lanes[i];
    // Get the name of the lane
    std::string laneName = createLaneName(segmentParent,
      lane.Id());
    // Create a the lanes in gazebo
    std::vector<ignition::math::Vector3d> roadPoints = GetLanePoints(lane);
    // Create a the lanes in gazebo
    gazebo::msgs::RoadPtr roadMsg =
      roadCreator.CreateRoadMessage(lane.Width(),
        laneName, roadPoints);
    roadCreator.PublishRoadMessage(roadMsg, scene->WorldVisual());
  }
}

void DynamicRender::LoadJunctions(
  std::vector<manifold::rndf::Lane> &lanes) {
  for (uint i = 0; i < lanes.size(); i++)
    LoadJunction(lanes[i]);
}

void DynamicRender::LoadJunction(const manifold::rndf::Lane &lane) {
  rendering::ScenePtr scene = rendering::get_scene();
  std::vector<manifold::rndf::Exit> exits = lane.Exits();
  for (uint i = 0; i < exits.size(); i++) {
    manifold::rndf::Exit &exit = exits[i];
    CreateJunction(exit, scene);
  }
}

void DynamicRender::LoadJunction(const manifold::rndf::Perimeter &perimeter) {
  rendering::ScenePtr scene = rendering::get_scene();
  std::vector<manifold::rndf::Exit> exits = perimeter.Exits();
  for (uint i = 0; i < exits.size(); i++) {
    manifold::rndf::Exit &exit = exits[i];
    CreateJunction(exit, scene);
  }
}

void DynamicRender::CreateJunction(manifold::rndf::Exit &exit,
  rendering::ScenePtr scene) {
  manifold::rndf::UniqueId &exitId = exit.ExitId();
  manifold::rndf::UniqueId &entryId = exit.EntryId();
  // Get the waypoint of the exitId
  manifold::rndf::Waypoint exitWaypoint =
    GetWaypointByUniqueId(exitId);
  manifold::rndf::Waypoint entryWaypoint =
    GetWaypointByUniqueId(entryId);
  // Create the points for the juction
  std::vector<ignition::math::Vector3d> junctionPoints;
  junctionPoints.push_back(projection.
    GetWaypointLocationInGlobal(exitWaypoint));
  junctionPoints.push_back(projection.
    GetWaypointLocationInGlobal(entryWaypoint));
  // Create a name for the junction
  std::string name = CreateJunctionName(exitId, entryId);
  // Draw the junction
  gazebo::msgs::RoadPtr roadMsg =
      roadCreator.CreateRoadMessage(4.0  /*lane.Width()*/,
        name, junctionPoints);
  roadCreator.PublishRoadMessage(roadMsg, scene->WorldVisual());
}

manifold::rndf::Waypoint DynamicRender::GetWaypointByUniqueId(
  const manifold::rndf::UniqueId &waypointId) {
  try {
    return GetWaypointInSegmentsByUniqueId(waypointId);
  }
  catch(gazebo::common::Exception &e) {
  }
  return GetWaypointInZonesByUniqueId(waypointId);
}

manifold::rndf::Waypoint DynamicRender::GetWaypointInSegmentsByUniqueId(
  const manifold::rndf::UniqueId &waypointId) {
  std::vector<manifold::rndf::Segment> &segments =
  rndfInfo->Segments();
  for (uint i = 0; i < segments.size(); i++) {
    manifold::rndf::Segment &segment = segments[i];
    if (segment.Id() != waypointId.X())
      continue;
    std::vector<manifold::rndf::Lane> &lanes =
      segment.Lanes();
    for (uint j = 0; j < lanes.size(); j++) {
      manifold::rndf::Lane &lane = lanes[j];
      if (lane.Id() != waypointId.Y())
        continue;
      std::vector<manifold::rndf::Waypoint> waypoints =
        lane.Waypoints();
      for (uint k = 0; k < waypoints.size(); k++) {
        manifold::rndf::Waypoint &waypoint = waypoints[k];
        if (waypoint.Id() == waypointId.Z())
          return waypoint;
      }
      gzthrow("Error. Waypoint " +
        createWaypointName(waypointId.X(),
          waypointId.Y(),
          waypointId.Z()) +
        " has not been found in segments.");
    }
    gzthrow("Error. Waypoint " +
      createWaypointName(waypointId.X(),
        waypointId.Y(),
        waypointId.Z()) +
      " has not been found in segments.");
  }
  gzthrow("Error. Waypoint " +
    createWaypointName(waypointId.X(),
      waypointId.Y(),
      waypointId.Z()) +
    " has not been found in segments.");
}

manifold::rndf::Waypoint DynamicRender::GetWaypointInZonesByUniqueId(
  const manifold::rndf::UniqueId &waypointId) {
  if (waypointId.Y() != 0) {
    gzthrow("Error. Waypoint " +
      createWaypointName(waypointId.X(),
        waypointId.Y(),
        waypointId.Z()) +
      " has not been found in zones.");
  }
  std::vector<manifold::rndf::Zone> &zones =
  rndfInfo->Zones();
  for (uint i = 0; i < zones.size(); i++) {
    manifold::rndf::Zone &zone = zones[i];
    if (zone.Id() != waypointId.X())
      continue;
    manifold::rndf::Perimeter &perimeter =
      zone.Perimeter();
    std::vector<manifold::rndf::Waypoint> &waypoints =
      perimeter.Points();
    for (uint j = 0; j < waypoints.size(); j++) {
      manifold::rndf::Waypoint waypoint =
        waypoints[j];
      if (waypoint.Id() == waypointId.Z())
        return waypoint;
    }
    gzthrow("Error. Waypoint " +
      createWaypointName(waypointId.X(),
        waypointId.Y(),
        waypointId.Z()) +
      " has not been found in zones.");
  }
  gzthrow("Error. Waypoint " +
    createWaypointName(waypointId.X(),
      waypointId.Y(),
      waypointId.Z()) +
    " has not been found in zones.");
}

manifold::rndf::Lane& DynamicRender::GetLaneByUniqueId(
  const manifold::rndf::UniqueId &waypointId) {
  std::vector<manifold::rndf::Segment> &segments =
  rndfInfo->Segments();
  for (uint i = 0; i < segments.size(); i++) {
    manifold::rndf::Segment &segment = segments[i];
    if (segment.Id() != waypointId.X())
      continue;
    std::vector<manifold::rndf::Lane> &lanes =
      segment.Lanes();
    for (uint j = 0; j < lanes.size(); j++) {
      manifold::rndf::Lane &lane = lanes[j];
      if (lane.Id() != waypointId.Y())
        continue;
      std::vector<manifold::rndf::Waypoint> waypoints =
        lane.Waypoints();
      for (uint k = 0; k < waypoints.size(); k++) {
        manifold::rndf::Waypoint &waypoint = waypoints[k];
        if (waypoint.Id() == waypointId.Z())
          return lane;
      }
      gzthrow("Error. Waypoint does not exist.");
    }
    gzthrow("Error. Lane has not been found.");
  }
  gzthrow("Error. Lane has not been found.");
}

void DynamicRender::LoadWaypoints(const int segmentParent,
  const int laneParent,
  std::vector<manifold::rndf::Waypoint> &waypoints) {
    std::vector<ignition::math::Pose3d> waypointPoses =
      GetWaypointsPoses(waypoints);

    for (uint i = 0; i < waypoints.size(); i++) {
      manifold::rndf::Waypoint &waypoint = waypoints[i];
      LoadWaypoint(segmentParent, laneParent,
        waypoint, waypointPoses[i],
        gazebo::gui::WayPointCreator::Type::DEFAULT);
    }
}
void DynamicRender::LoadWaypoints(const int segmentParent,
  const int laneParent,
  std::vector<manifold::rndf::Waypoint> &waypoints,
  const gazebo::gui::WayPointCreator::Type type) {
    std::vector<ignition::math::Pose3d> waypointPoses =
      GetWaypointsPoses(waypoints);

    for (uint i = 0; i < waypoints.size(); i++) {
      LoadWaypoint(segmentParent, laneParent,
        waypoints[i], waypointPoses[i], type);
    }
}

void DynamicRender::LoadWaypoint(const int segmentParent,
  const int laneParent,
  manifold::rndf::Waypoint &waypoint,
  ignition::math::Pose3d &pose,
  const gazebo::gui::WayPointCreator::Type type) {
    rendering::ScenePtr scene = rendering::get_scene();
    // Get the name of the waypoints and its position
    std::string name = createWaypointName(segmentParent,
      laneParent, waypoint.Id());
    ignition::math::Vector3d position =
      projection.GetWaypointLocationInGlobal(waypoint);
    // Load the waypoint in gazebo
    gazebo::msgs::VisualPtr msg = waypointCreator.
      CreateWayPointMessage(name,
        scene->WorldVisual()->GetName(),
        pose,
        1.0,
        ignition::math::Vector3i(255, 0, 0),
        type);
    waypointCreator.PublishVisualMessage(msg);
}

ignition::math::Pose3d DynamicRender::GetWaypointPose(
  manifold::rndf::Waypoint &waypoint,
  manifold::rndf::Waypoint &nextWaypoint) {
    ignition::math::Vector3d waypointPosition =
      projection.GetWaypointLocationInGlobal(waypoint);
    ignition::math::Vector3d nextWaypointPosition =
      projection.GetWaypointLocationInGlobal(nextWaypoint);
    ignition::math::Vector3d direction =
      nextWaypointPosition - waypointPosition;
    // Calculate Euler angles and create quaterion
    double yaw, roll, pitch;
    yaw = std::atan2(direction.Y(), direction.X()) -
      ignition::math::Angle::HalfPi.Radian();
    roll = std::atan2(direction.Z(), direction.Y());
    pitch = std::atan2(direction.Z(), direction.X());
    ignition::math::Quaterniond quaternion(roll, pitch, yaw);

    ignition::math::Pose3d pose;
    pose.Pos() = waypointPosition;
    pose.Rot() = quaternion;
    return pose;
}

std::vector<ignition::math::Pose3d> DynamicRender::GetWaypointsPoses(
  std::vector<manifold::rndf::Waypoint> &waypoints) {
    std::vector<ignition::math::Pose3d> poses;
    for (uint i = 0; i < waypoints.size()-1; i++) {
      ignition::math::Pose3d pose =
        GetWaypointPose(waypoints[i], waypoints[i+1]);
      poses.push_back(pose);
    }
    ignition::math::Pose3d pose(poses.back());
    pose.Pos() = projection.
      GetWaypointLocationInGlobal(waypoints.back());
    poses.push_back(pose);
    return poses;
}

std::string DynamicRender::createSegmentName(const int segmentId) {
  return "segment_" + std::to_string(segmentId);
}

std::string DynamicRender::createLaneName(const int segmentParentId,
  const int laneId) {
  return "lane_" + std::to_string(segmentParentId) +
    "_" + std::to_string(laneId);
}

std::string DynamicRender::createWaypointName(const int segmentParentId,
  const int laneParentId,
  const int waypointId) {
    return "waypoint_" + std::to_string(segmentParentId) +
      "_" + std::to_string(laneParentId) +
      "_" + std::to_string(waypointId);
}

std::string DynamicRender::CreateJunctionName(
  manifold::rndf::UniqueId &exitId,
  manifold::rndf::UniqueId &entryId) {
    return "junction_" + std::to_string(exitId.X()) + "." +
      std::to_string(exitId.Y()) + "." +
      std::to_string(exitId.Z()) + "_" +
      std::to_string(entryId.X()) + "." +
      std::to_string(entryId.Y()) + "." +
      std::to_string(entryId.Z());
}

void DynamicRender::OnPublishWaypoints(
  const manifold::rndf::UniqueId &wpId) {
    manifold::rndf::Lane lane;
    try {
      lane = GetLaneByUniqueId(wpId);
    }
    catch(gazebo::common::Exception &e) {
      gzerr << e;
      return;
    }
    // Get all the waypoints positions and interpolated
    // points positions of the lane
    std::vector<ignition::math::Vector3d> lanePoints =
      GetLanePoints(lane);
    // Get the exits and entries of the lane, then create vectors
    // to keep the positions of each one
    std::vector<manifold::rndf::Exit> &exits = lane.Exits();
    std::vector<ignition::math::Vector3d> exitPositions;
    std::vector<ignition::math::Vector3d> entryPositions;
    std::vector<manifold::rndf::UniqueId> exitIds;
    std::vector<manifold::rndf::UniqueId> entryIds;

    for (uint i = 0; i < exits.size(); i++) {
      manifold::rndf::Exit exit = exits[i];
      exitIds.push_back(exit.ExitId());
      entryIds.push_back(exit.EntryId());
      try {
        manifold::rndf::Waypoint wp =
          GetWaypointByUniqueId(exit.ExitId());
        exitPositions.push_back(
          projection.GetWaypointLocationInGlobal(wp));
        wp = GetWaypointByUniqueId(exit.EntryId());
        entryPositions.push_back(
          projection.GetWaypointLocationInGlobal(wp));
      }
      catch(gazebo::common::Exception &e) {
        gzerr << e;
        return;
      }
    }
}

std::vector<ignition::math::Vector3d> DynamicRender::GetLanePoints(
  manifold::rndf::Lane &lane) {
    std::vector<manifold::rndf::Waypoint> &waypoints = lane.Waypoints();
    // Get the waypoint positions
    std::vector<ignition::math::Vector3d> waypointPositions;
    for (uint i = 0; i < waypoints.size(); i++) {
      ignition::math::Vector3d position =
        projection.GetWaypointLocationInGlobal(waypoints[i]);
      waypointPositions.push_back(position);
    }
    // Create a lane in gazebo
    return roadCreator.InterpolateRoad(waypointPositions,
        interpolationDistance);
}

std::string DynamicRender::CreatePerimeterName(const int zoneId) {
  return "perimeter_" + std::to_string(zoneId);
}

void DynamicRender::LoadZones(
  std::vector<manifold::rndf::Zone> &zones) {
    for (uint i = 0; i < zones.size(); i++) {
      manifold::rndf::Zone &zone = zones[i];
      LoadPerimeter(zone.Id(), zone.Perimeter());
      LoadParkingSpots(zone.Id(), zone.Spots());
    }
}

void DynamicRender::LoadPerimeter(const int zoneId,
  manifold::rndf::Perimeter &perimeter) {
    std::vector<manifold::rndf::Waypoint> waypoints =
      perimeter.Points();
    CreatePerimeter(waypoints);
    LoadWaypoints(zoneId, 0, waypoints,
      gazebo::gui::WayPointCreator::Type::PERIMETER);
    LoadJunction(perimeter);
}

void DynamicRender::LoadParkingSpots(const int zoneId,
  std::vector<manifold::rndf::ParkingSpot> &spots) {
  for (uint i = 0; i < spots.size(); i++) {
    manifold::rndf::ParkingSpot spot = spots[i];
    std::vector<manifold::rndf::Waypoint> waypoints =
      spot.Waypoints();
    LoadWaypoints(zoneId, spot.Id(), waypoints,
      gazebo::gui::WayPointCreator::Type::SPOT);
  }
}

void DynamicRender::CreatePerimeter(
  std::vector<manifold::rndf::Waypoint> &waypoints) {
  std::vector<ignition::math::Vector3d> verteces;
  for (uint i = 0; i < waypoints.size(); i++) {
    verteces.push_back(
      projection.GetWaypointLocationInGlobal(waypoints[i]));
  }
  perimeterCreator.Create("Gazebo/Black", verteces);
}

}
}
